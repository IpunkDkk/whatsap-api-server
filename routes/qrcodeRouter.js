const express = require('express');
const router = express.Router();
const qrcode = require('qrcode');
const { Client, LocalAuth } = require('whatsapp-web.js');
const {OpenAIApi, Configuration} = require('openai');
require ('dotenv').config();

// OpenAI API credentials
const configuration = new Configuration({
    organization: "org-LowWZ9YVD9OxpjgotnEUE7jh",
    apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);

const createSessions = (id, describtion) => {
    return new Client({
        authStrategy: new LocalAuth()
    })
}

// Endpoint to generate a QR code for the user to scan and authenticate the client
router.get('/qr-code', (req, res) => {
    try {
        // Initialize a new WhatsApp client
        const client = createSessions("agung", "test")
        client.initialize();

        client.on('qr', (qr) => {
            console.log('QR RECEIVED', qr);
            qrcode.toDataURL(qr, (err, url) => {
                res.send(`<img src="${url}">`);
            });
        });
    } catch (error) {
        console.error(error);
        res.sendStatus(500);
    }
});

// Endpoint to handle incoming messages
router.post('/messages', (req, res) => {
    const client = createSessions("agung", "test")

    const { body } = req;

    // Parse the incoming message and do something with it
    // Example: send an automated reply
    const message = client.message(body.from, body.body);
    message.reply('Thanks for your message! We will get back to you soon.');

    res.sendStatus(200);
});

// Start the client and listen for incoming messages
const client = createSessions("agung", "test")
client.on('ready', () => {
    console.log('Client is ready to receive messages.');
    client.on('message', async message => {
        if (message.body === '!ping') {
            message.reply('pong');
        } else if (message.body === 'kamu cinta aku gak?') {
            message.reply('nggak lah ngapain njir. huek');
        }else{
        console.log(`Received message from ${message.from}: ${message.body}`);
        const prompt = `User: ${message.body}\nChatGPT:`;
        const chatgptResponse = await openai.createCompletion({
            model: 'text-davinci-003',
            prompt,
            max_tokens: 2000,
            temperature: 0,
        })
            .then((response) => {
                console.log(response.data.choices[0].text);
                message.reply(response.data.choices[0].text);
            })
            .catch(err => {
                console.log(err.message);
            })
        // Send a reply to the user with ChatGPT's response
        }

    });
});

client.initialize();

module.exports = router;
