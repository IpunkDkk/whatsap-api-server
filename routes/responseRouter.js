const express = require('express');
const router = express.Router();
const Validator = require('fastest-validator');
const v = new Validator;

const {responses} = require('../models');

router.get('/', async (req,res)=>{
    const response = await responses.findAll()
    return res.json(response);
})

router.get('/show/:id', async (req, res) => {
    const id = req.params.id
    let response = await responses.findByPk(id);
    if (!response){
        return res.json("item not found")
    }
    return res.json(response)
})

router.get('/create', async (req, res) => {
    const schema = {
        key: "string",
        response:"string"
    }
    const data = req.query
    console.log(data)

    const validate = v.validate(data, schema)

    if (validate.length){
        return res.status(400).json(validate)
    }

    const  response = await responses.create(data);
    return res.json(response)
})

router.get('/update/:id', async (req, res) => {
    const schema = {
        key: "string|optional",
        response:"string|optional"
    }
    const data = req.query
    const id = req.params.id
    let response = await responses.findByPk(id);
    if (!response){
        return res.json("item not found")
    }
    const validate = v.validate(data, schema);
    if (validate.length){
        return res.status(400).json(validate)
    }
    response = await response.update(data)
    return res.json(response)
})

router.get('/delete/:id', async (req, res) => {
    const id = req.params.id
    let response = await responses.findByPk(id);
    if (!response) {
        return res.json("item not found")
    }
    await response.destroy()
    return res.json("item deleted")
})

module.exports = router