'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

     await queryInterface.createTable('responses', {
       id: {
         type: Sequelize.INTEGER,
         primaryKey: true,
         autoIncrement:true,
         allowNull:false
       },
       key: {
         type: Sequelize.STRING,
         allowNull: false
       },
       response: {
         type: Sequelize.TEXT,
         allowNull: false
       },
       createdAt: {
         type: Sequelize.DATE,
         allowNull:false
       },
       updatedAt:{
         type: Sequelize.DATE,
         allowNull:false
       }
     });

  },

  async down (queryInterface, Sequelize) {
     await queryInterface.dropTable('responses');
  }
};
