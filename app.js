require ('dotenv').config();
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const db = require("./models")

const responseRouter = require('./routes/responseRouter');
const qrcodeRouter = require('./routes/qrcodeRouter');

const app = express();


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/response', responseRouter);
app.use('/qrcode', qrcodeRouter);




module.exports = app;
